#!/bin/bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Load necessary modules
module purge
module load anaconda/3/2023.03
module load pytorch/gpu-cuda-11.6/2.1.0
module load tensorboard/2.13.0

# If venv deoes not exist create it
if [ ! -f ${ROOT_DIR}/venv/bin/activate ]
then
    echo "venv does not exist - Creating it..."
    python -m venv --system-site-packages venv
fi

# Activate venv
source ${ROOT_DIR}/venv/bin/activate

# If local ipython kernel for venv deoes not exist create it
if [ ! -d ${HOME}/.local/share/jupyter/kernels/hpo ] && [[ "${VIRTUAL_ENV}" == *"${ROOT_DIR}/venv" ]]
then
    echo "Installing local ipython kernel..."
    python -m ipykernel install --user --name hpo --display-name "Python (hpo-venv)"
fi

# Create directories for SLURM logs if not existing
mkdir -p ${ROOT_DIR}/out/optuna
mkdir -p ${ROOT_DIR}/out/ray

import torch
import torch.nn.functional as F
import torch.optim as optim

from models import MLP
from utils import train_epoch, eval_model, get_FashionMNIST
from argparse import ArgumentParser


def train_fn(args):
    # HyperParameters
    lr = args.lr
    hidden = args.hidden
    bs = args.bs
    epochs = args.epochs

    # Model and Training configuration
    device = torch.device("cuda")
    model = MLP(hidden=hidden).to(device)
    optimizer = optim.SGD(params=model.parameters(), lr=lr)
    loss = F.nll_loss
    
    # Dataset
    train_loader, valid_loader = get_FashionMNIST(
        batch_size=bs,
        device=device
    )    

    # Training loop
    for epoch in range(1, epochs + 1):
        train_epoch(model, device, train_loader, optimizer,
                    loss, epoch, verbose=True)
        _, valid_acc = eval_model(model, device, valid_loader,
                                 loss, verbose=True)

    print(f"Final test accuracy: {valid_acc:.2f}")

if __name__ == '__main__':
    parser = ArgumentParser(add_help=False)
    parser.add_argument('--bs', type=int, default=64,
                        help='input batch size for training')
    parser.add_argument('--epochs', type=int, default=10,
                        help='number of epochs to train')
    parser.add_argument('--lr', type=float, default=1e-3,
                        help='learning rate')
    parser.add_argument('--hidden', type=int, default=128,
                        help='Hidden units in the first layer')

    args = parser.parse_args()

    train(args)
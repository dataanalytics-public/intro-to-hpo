#############################################
# Adapted from PyTorch official docummentation:
# https://github.com/pytorch/examples/blob/main/mnist/README.md
#############################################

import torch
import torch.nn.functional as F
from torchvision import datasets, transforms

def train_epoch(model, device, train_loader, optimizer, loss, epoch, log_interval=100, verbose=False):
    model.train()
    epoch_loss = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        train_loss = loss(output, target)
        epoch_loss += train_loss.item()
        
        train_loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0 and verbose:
            print(f"Train Epoch: {epoch} [{batch_idx * len(data)}/{len(train_loader.dataset)}"+
                  f"({100. * batch_idx / len(train_loader):.0f}%)]\tLoss: {train_loss.item():.6f}")
    return epoch_loss / len(train_loader)

def eval_model(model, device, data_loader, loss, verbose=False):
    model.eval()
    mean_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in data_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            mean_loss += loss(output, target).item()
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    mean_loss /= len(data_loader)
    accuracy = 100. * correct / len(data_loader.dataset)

    if verbose:
        print(f"\nEvaluation: Average loss: {mean_loss:.4f},"+
              f"Accuracy: {correct}/{len(data_loader.dataset)}({accuracy:.1f}%)\n")
    return mean_loss, accuracy


def get_FashionMNIST(batch_size, device, path='../data'):
    train_kwargs = {'batch_size': batch_size, 'num_workers': 8}
    valid_kwargs = {'batch_size': batch_size, 'num_workers': 8}
    
    if device.type == 'cuda':
        cuda_kwargs = {'pin_memory': True,
                       'shuffle': True,
                       'prefetch_factor': 10}
        train_kwargs.update(cuda_kwargs)
        valid_kwargs.update(cuda_kwargs)

    dataset1 = datasets.FashionMNIST(path, train=True, download=True,
                       transform=transforms.ToTensor())
    dataset2 = datasets.FashionMNIST(path, train=False, download=True,
                       transform=transforms.ToTensor())
    train_loader = torch.utils.data.DataLoader(dataset1,**train_kwargs)
    valid_loader = torch.utils.data.DataLoader(dataset2, **valid_kwargs)

    return train_loader, valid_loader
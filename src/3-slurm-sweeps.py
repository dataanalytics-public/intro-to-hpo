import torch
import torch.nn.functional as F
import torch.optim as optim

from models import MLP
from utils import train_epoch, eval_model, get_FashionMNIST

from pathlib import Path
import slurm_sweeps as ss
import wandb


def train(cfg: dict):
    device = torch.device(f"cuda")
    model = MLP(hidden=cfg["hidden"]).to(device)
    optimizer = optim.SGD(params=model.parameters(), lr=cfg["lr"])
    loss = F.nll_loss

    train_loader, valid_loader = get_FashionMNIST(
        batch_size=cfg["bs"],
        device=device,
        path=cfg["data"]
    )

    wandb.init(project="MeetTheMPCDF", group="slurm-sweeps", config=cfg, mode="offline")

    # Training loop
    for epoch in range(1, cfg["epochs"] + 1):
        train_loss = train_epoch(model, device, train_loader, optimizer, loss, epoch, verbose=True)
        val_loss, val_acc = eval_model(model, device, valid_loader, loss, verbose=True)

        wandb.log({"train_loss": train_loss, "val_loss": val_loss, "val_acc": val_acc, "Epoch": epoch})
        ss.log({"val_acc": val_acc}, iteration=epoch)

    wandb.finish()


if __name__ == "__main__":
    experiment = ss.Experiment(
        train=train,
        cfg={
            "bs": 64,
            "epochs": 10,
            "lr": ss.LogUniform(1e-3, 1),
            "hidden": ss.Choice(list(range(64, 512, 64))),
            "data": str(Path("../data").resolve()),
        },
        asha=ss.ASHA(metric="val_acc", mode="max"),
        backend=ss.SlurmBackend(args="--gres=gpu:a100:1"),
    )

    experiment.run(n_trials=200)

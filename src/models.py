import torch
import torch.nn as nn
import torch.nn.functional as F

class MLP(nn.Module):
    def __init__(self, in_features=784, hidden=128, n_classes=10):
        super(MLP, self).__init__()
        self.dense1   = nn.Linear(in_features, hidden)
        self.logits   = nn.Linear(hidden, n_classes)

    def forward(self, x):
        x = torch.flatten(x, start_dim=1)
        x = self.dense1(x)
        x = F.relu(x)
        x = self.logits(x)
        output = F.log_softmax(x, dim=1)
        return output

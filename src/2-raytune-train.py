import torch
import torch.nn.functional as F
import torch.optim as optim

from models import MLP
from utils import train_epoch, eval_model, get_FashionMNIST
from argparse import ArgumentParser

import os
import ray
from ray import train, tune

def train_fn(config, data_dir):
    # HyperParameters
    lr = config['lr']
    hidden = config['hidden']
    bs = 64
    epochs = 10

    # Model and Training configuration
    device = torch.device("cuda")
    model = MLP(hidden=hidden).to(device)
    optimizer = optim.SGD(params=model.parameters(), lr=lr)
    loss = F.nll_loss

    # Dataset
    train_loader, valid_loader = get_FashionMNIST(
        batch_size=bs,
        device=device,
        path=data_dir
    )

    # Training loop
    for epoch in range(1, epochs + 1):
        train_epoch(model, device, train_loader, optimizer,
                    loss, epoch, verbose=False)
    _, valid_acc = eval_model(model, device, valid_loader,
                             loss, verbose=False)

    train.report({'accuracy': valid_acc})

if __name__ == '__main__':
    parser = ArgumentParser(add_help=False)
    # TUNE
    parser.add_argument('--num-gpus', type=float, default=1,
                        help='number of GPUs per ray workers')
    parser.add_argument('--num-cpus', type=int, default=4,
                        help='number of CPUs per ray workers')
    parser.add_argument('--num-samples', type=int, default=20,
                        help='number of trails')
    parser.add_argument('--log-dir', default='./ray_results')
    parser.add_argument('--data-dir', default='../data')
    args = parser.parse_args()

    ray.init()

    # Define params space
    search_space = {
        "lr": tune.loguniform(1e-3,1),
        "hidden": tune.choice(range(64, 512, 64)),
    }

    # Tuning configuration
    tuner = tune.Tuner(
        tune.with_resources(
            tune.with_parameters(train_fn, data_dir=args.data_dir),
            resources={"cpu": args.num_cpus, "gpu": args.num_gpus}
        ),
        param_space=search_space,
        tune_config=tune.TuneConfig(
            metric='accuracy',
            mode='max',
            num_samples=args.num_samples,
        ),
        run_config=train.RunConfig(
            name='lr-hidden',
            storage_path=args.log_dir,
        )
    )

    results = tuner.fit()
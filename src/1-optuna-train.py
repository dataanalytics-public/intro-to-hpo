import torch
import torch.nn.functional as F
import torch.optim as optim

from models import MLP
from utils import train_epoch, eval_model, get_FashionMNIST
from argparse import ArgumentParser
import optuna

def train_fn(trial):
    # HyperParameters
    lr = trial.suggest_float('lr', 1e-3, 1, log=True)
    hidden = trial.suggest_int('hidden', 64, 512, 64)
    bs = 64
    epochs = 10
    
    # Model and Training configuration
    device = torch.device("cuda")
    model = MLP(hidden=hidden).to(device)
    optimizer = optim.SGD(params=model.parameters(), lr=lr)
    loss = F.nll_loss
    
    # Dataset
    train_loader, valid_loader = get_FashionMNIST(
        batch_size=bs,
        device=device
    )

    # Training loop
    for epoch in range(1, epochs + 1):
        train_epoch(model, device, train_loader, optimizer,
                    loss, epoch, verbose=False)
    _, valid_acc = eval_model(model, device, valid_loader,
                             loss, verbose=False)

    return valid_acc

if __name__ == '__main__':
    parser = ArgumentParser(add_help=False)
    parser.add_argument('--n-trails', type=int, default=20,
                        help='Number of trails to run')

    args = parser.parse_args()

    study = optuna.create_study(direction='maximize',
                            storage="sqlite:///optuna/MLP.db",
                            study_name="lr-hidden",
                            load_if_exists=True)
    study.optimize(train_fn, n_trials=args.n_trails)
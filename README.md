# Install requirements

## On RAVEN

Source the `activate-venv.sh` script:
```
source activate-venv.sh
```
This will load the necessary modules and, if not already installed, create a virtual environment `venv` and a ipython kernel `hpo`.

Now the other requirements can be installed:
```
pip install -r requirements
```

To deactivate the environment execute the `deactivate` command. To reactivate it source again the `activate-venv.sh` script.

## General
Install the requirements via `pip`:
```
pip install -r requirements
```